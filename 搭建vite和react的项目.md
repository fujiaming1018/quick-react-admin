# 搭建vite+react项目

## vite

``` bash
pnpm create vite my-vue-app -- --template react-ts
```

## ui组件库


``` bash
pnpm i antd -D
```

## 路由
``` bash
pnpm i react-router-dom -D
```

## 状态管理
``` bash
pnpm i @reduxjs/toolkit react-redux -D
```

### 持久化：
``` bash
pnpm i redux-persist -D
```


## css
``` bash
pnpm i less -D
```

## axios 
``` bash
pnpm i axios -D
```
