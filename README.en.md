<div align="center"> <a href="https://gitee.com/zhanglp520/quick-vue3-admin.git"> <img alt="quick-vue3-admin Logo" width="200" height="200" src="https://raw.githubusercontent.com/wiki/zhanglp520/quick-vue3-admin/logo.png"> </a> <br> <br>
</div>

**English** | [Chinese](./README.md)

# quick-react-admin

## Introduction

quick-react-admin is a free, open-source framework for quickly building backend systems. This framework is based on the latest mainstream technologies such as vite5, react, antd, @reduxjs/toolkit, and react-router-dom, and encapsulates universal components to facilitate developers in improving work efficiency. Later on, it will also be maintained and updated through version upgrades, allowing developers to have a long-term and stable scaffold. Our team also provides various business projects developed based on the Quick framework, such as order management system, scheduling management system, chat system, audio and video system, monitoring system, mall system, Internet of Things platform, takeout system, ERP system, CMR system, OA system, logistics management system, CRM management system, and other commonly used business systems. If there are any related requirements, please contact the administrator.

## Software features

1. Using preface techniques such as vite5, React8, Redux, @ Reduxjs/toolkit, React outer dom, Antd, and Typescript
2. Encapsulated universal components
3. Provides common functional modules for system management
4. Provide permission management module
5. Dynamic menu technology
6. Dynamic routing technology
7. Use JWT authentication
8. Using middleware for exception handling
9. Front and rear end separation
10. Provide free assistance channels
11. Provide a free framework demonstration environment
12. The official provides a stable and long-term maintenance node light backend interface project
13. Using the Nestjs framework on the backend
14. Use Restful Interface Specification
15. Personal data modification and password modification function
16. Token expiration function
17. Refresh token function
18. Modular management
19. Using mvc architecture and multi-layer design ideas
20. Using the typeorm framework, it can support databases and multiple types of databases

## Preview

-   [quick-react-admin]（ https://react.quick.ainiteam.com/ ）- Framework Demo Version 1.0

Demo account password: admin/123456

## Development documentation

-[quick admin]（ https://doc.quick.ainiteam.com/react ）- quick-react-admin development document

## Interface documentation

-[Quick Interface Document]（ https://console-docs.apipost.cn/preview/0e11a2eb3c3883a7/4fff7a394c074ac7 ）- Quick Interface Document Version 1.0

-[Quick Interface Document]（ https://console-docs.apipost.cn/preview/52de13c4d013470f/e5aa6f10d52601f7 ）- Quick Interface Document Version 2.0

## Installation and use

-   Clone Project

```bash
github： git clone https://gitee.com/zhanglp520/quick-react-admin.git

gitee：git clone https://gitee.com/zhanglp520/quick-react-admin.git

```

-   Switch Taobao image

```Bash

npm config set registry https://registry.npm.taobao.org

```

-   Switch NPM

```Bash
npm config set registry https://registry.npmjs.org

```

-   Verification of successful switching

```Bash

npm config get registry

```

-   Install pnpm

```Bash

npm install - g pnpm

```

-   Install Project Dependency Package

```Bash

pnpm install

```

If the installation fails, delete pnpm lock.yaml and node_modules, as well as the. pnpm store directory on drive D, and reinstall

-   Run

```Bash

pnpm run dev

```

After running the command execution service, the browser enters: http://localhost:3100/

-   Eslint detection

```Bash

PNPM run lint detection and attempt to repair

```

-   Pack and Go Live

```Bash

pnpm run build

```

-   Preview after packaging

```Bash
pnpm run preview

```

## Backend interface

-   Official backend interface project express version:
    https://gitee.com/zhanglp520/quick-node-express.git Interface v1 version

-   Officially provided backend interface project nestjs version:
    https://github.com/zhanglp520/quick-node-Nestjs.git Interface v2 version

## Seeking help

1. View official help documents
2. Initiate Issue
3. Add Quick Framework QQ Group:

-   Group 1: 528166164(vue)
-   Group 2: 485013155(react)
-   Group 3: 558795174(electron)

## License

[MIT © quick-react-admin](./LICENSE)

## Participation contribution

1. Fork warehouse
2. Create a Feat_xxx branch
3. Submit code, submit code rules: (^ (Fix | Update | New | Breaking | Docs | Build | Upgrade | Chor)?: {1,72})
4. Create a new Pull Request
5. Click on the star button
6. If you need to join open source, please contact the administrator
7. If you feel that this project is helpful to you, you can help the author buy a cup of coffee to show your support!
   ![donate](https://raw.githubusercontent.com/wiki/zhanglp520/quick-vue3-admin/20230430121236.png)
