import { defaultVersion } from "../index";
/*
 *@Description: 系统管理模块api
 *@Author: 土豆哥
 *@Date: 2022-11-28 11:54:32
 */
const parentModule = "/develop";
const apiUrl = `/develop${defaultVersion}${parentModule}`;
export const generator = `${apiUrl}/generators`;
